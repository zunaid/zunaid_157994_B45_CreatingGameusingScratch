<?php

namespace App\BookTitle;

use App\Message\Message;

use App\Utility\Utility;

use PDO;


use App\Model\Database as DB;

class BookTitle extends  DB
{
    private  $id;

    private  $book_name;

    private  $author_name;

    public function setData($allPostData=null)
    {

        if (array_key_exists("id", $allPostData)) {


            $this->id = $allPostData['id'];
        }

        if (array_key_exists("bookName", $allPostData)) {


            $this->book_name = $allPostData['bookName'];
        }

        if (array_key_exists("authorName", $allPostData)) {


            $this->author_name = $allPostData['authorName'];
        }

    }

        public function store(){

            $arrData = array($this->book_name,$this->author_name);
            $query= 'INSERT INTO book_title (book_name,author_name) VALUES (?,?)';



              $STH = $this->DBH->prepare($query);

              $result = $STH->execute($arrData);

                 if($result){

                     Message::setMessage("Success!Data has been stored successfully!");

                 }
                 else{

                     Message::setMessage("Failed!Data has not been stored!");
                 }

            utility :: redirect('index.php');



        }

    public function view()
    {

        $sql = "Select * from book_title where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from book_title where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed()
    {

        $sql = "Select * from book_title  where soft_deleted='Yes'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update(){

        $arrData = array($this->book_name,$this->author_name);
        $query= 'UPDATE  book_title SET book_name = ?,author_name = ? WHERE id='.$this->id;



        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been updated successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been updated!");
        }

        utility :: redirect('index.php');



    }

    public function trash(){

        $arrData = array("Yes");
        $query= 'UPDATE  book_title SET soft_deleted = ? WHERE id='.$this->id;



        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been trashed successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been trashed!");
        }

        utility :: redirect('trashed.php');

    }

    public function recover(){

        $arrData = array("No");
        $query= 'UPDATE  book_title SET soft_deleted = ? WHERE id='.$this->id;



        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been recovered successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been recovered!");
        }

        utility :: redirect('index.php');

    }

    public function delete(){

        $sql= "DELETE FROM book_title WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result){

            Message::setMessage("Success! Book name has been deleted!");
        }

        else{

            Message::setMessage("Failed! Book name has not been deleted");
        }

        utility ::redirect('index.php');

    }

    public function indexPaginator($page,$itemsPerPage){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM book_title WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }






}