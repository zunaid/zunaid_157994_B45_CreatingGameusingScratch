<?php


namespace App\Gender;

use App\Message\Message;

use App\Utility\Utility;

use PDO;


use App\Model\Database as DB;

class Gender extends DB

{
    private $id;

    private $Name;

    private $gender;

    public function setData($allPostData=null){

        if (array_key_exists("id", $allPostData)) {


            $this->id = $allPostData['id'];
        }

        if (array_key_exists("name", $allPostData)) {


            $this->Name = $allPostData['name'];
        }

        if (array_key_exists("GENDER", $allPostData)) {


            $this->gender = $allPostData['GENDER'];
        }

        utility :: redirect('index.php');

    }

    public function store(){

        $Data = array($this->Name,$this->gender);

        $query= 'INSERT INTO gender_info (person_name, gender) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($Data);

        if($result){

            Message::setMessage("Success!Gender value has been stored successfully!");

        }
        else{

            Message::setMessage("Failed!Gender value has not been stored!");
        }

        utility :: redirect('index.php');

    }
    public function view()
    {

        $sql = "SELECT * FROM gender_info WHERE id=". $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "SELECT * FROM gender_info WHERE soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "SELECT * FROM gender_info WHERE soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update()
    {

        $Data = array($this->Name, $this->Gender);

        $query = 'UPDATE gender_info SET person_name=?, gender=? WHERE id=' . $this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($Data);

        if ($result) {

            Message::setMessage("Success!Gender value has been updated successfully!");

        } else {

            Message::setMessage("Failed!Gender value has not been updated!");
        }

        utility:: redirect('index.php');


    }


    public function trash()
    {

        $Data = array("Yes");

        $query = 'UPDATE gender_info SET soft_deleted=? WHERE id=' . $this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($Data);

        if ($result) {

            Message::setMessage("Success!Gender value has been trashed successfully!");

        } else {

            Message::setMessage("Failed!Gender value has not been trashed!");
        }

        utility:: redirect('trashed.php');


    }

    public function recover()
    {

        $arrData = array("No");

        $query = 'UPDATE gender_info SET soft_deleted=? WHERE id=' . $this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Gender value has been recovered successfully!");

        } else {

            Message::setMessage("Failed!Gender value has not been recovered!");
        }

        utility:: redirect('index.php');


    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM gender_info WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM gender_info WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }





    }