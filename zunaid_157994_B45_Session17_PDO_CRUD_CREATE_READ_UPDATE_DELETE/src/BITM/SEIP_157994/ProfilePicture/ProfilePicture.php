<?php

namespace App\ProfilePicture;

use App\Message\Message;

use App\Utility\Utility;

use PDO;

use App\Model\Database as DB;




class ProfilePicture extends DB
{
    private $id;

    private $Name;

    private $profile_image;

    public function setData($allPostData = null)
    {

        if (array_key_exists("id", $allPostData)) {


            $this->id = $allPostData['id'];
        }

        if (array_key_exists("name", $allPostData)) {


            $this->Name = $allPostData['name'];
        }


        if (array_key_exists("profileImage", $allPostData)) {


            $this->profile_image = $allPostData['profileImage'];
        }
    }


    public function store()
    {


        $picture = array($this->Name,$this->profile_image);

        $query = 'INSERT INTO profile_picture (person_name,picture_name) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($picture);

        if ($result) {

            Message::setMessage("Success!Picture has been stored successfully!");

        } else {

            Message::setMessage("Failed!Picture has not been stored!");
        }

        utility :: redirect('index.php');
    }

    public function view()
    {

        $sql = "Select * from profile_picture where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from profile_picture where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "Select * from profile_picture where soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update()
    {


        $picture = array($this->Name,$this->profile_image);

        $query = 'UPDATE profile_picture SET person_name=?,picture_name=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($picture);

        if ($result) {

            Message::setMessage("Success!Picture has been updated successfully!");

        } else {

            Message::setMessage("Failed!Picture has not been updated!");
        }

        utility :: redirect('index.php');
    }

    public function trash()
    {


        $arrData = array("Yes");

        $query = 'UPDATE profile_picture SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Picture has been trashed successfully!");

        } else {

            Message::setMessage("Failed!Picture has not been trashed!");
        }

        utility :: redirect('trashed.php');
    }

    public function recover()
    {


        $arrData = array("No");

        $query = 'UPDATE profile_picture SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Picture has been recovered successfully!");

        } else {

            Message::setMessage("Failed!Picture has not been recovered!");
        }

        utility:: redirect('index.php');
    }

    public function delete(){

        $sql = "DELETE FROM porfile_picture WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result){

            Message::setMessage("Success! Data has been deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been deleted!");
        }

        utility :: redirect('index.php');
    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM profile_picture WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM profile_picture WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }
}




