<?php



namespace App\Email;

use App\Message\Message;

use App\Utility\Utility;

use PDO;


use App\Model\Database as DB;

class Email extends DB
{
     private $id;
     private $name;
     private $Email;

     public function setData($allPostData=null){

            if(array_key_exists("id",$allPostData)){

             $this->id = $allPostData['id'];
               }


             if(array_key_exists("Name",$allPostData)){

             $this->name = $allPostData['Name'];
             }



             if(array_key_exists("EMail",$allPostData)){

             $this->Email = $allPostData['EMail'];
             }


     }

    public function store(){

           $arrData = array($this->name,$this->Email);

           $query= 'INSERT INTO email (person_name, email) VALUES (?,?)';

           $STH = $this->DBH->prepare($query);

           $result = $STH->execute($arrData);

           if($result){

                    Message::setMessage("Success!Data has been stored successfully!");

             }
           else{

                    Message::setMessage("Failed!Data has not been stored!");
            }



            utility :: redirect('index.php');


   }

    public function view()
    {

        $sql = "Select * from email where id=" . $this->Id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from email where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "Select * from email where soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update(){

        $arrData = array($this->name,$this->Email);

        $query= 'UPDATE email SET  person_name=?, email=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been updated successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been updated!");
        }



        utility :: redirect('index.php');


    }

    public function trash(){

        $arrData = array("Yes");

        $query= 'UPDATE email SET  soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been trashed successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been trashed!");
        }



        utility :: redirect('trashed.php');


    }

    public function recover(){

        $arrData = array("No");

        $query= 'UPDATE email SET  soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message::setMessage("Success!Data has been recovered successfully!");

        }
        else{

            Message::setMessage("Failed!Data has not been recovered!");
        }



        utility :: redirect('index.php');


    }

    public function delete(){

        $sql = 'DELETE FROM email WHERE id='. $this->id;

        $result = $this->DBH->exec($sql);

        if($result){

            Message::setMessage("Success! Email has been deleted successfully");
        }
        else{
            Message::setMessage("Failed! Email has not been deleted");
        }

        utility :: redirect('index.php');
    }

     public function indexPaginator($page=1,$itemsPerPage=3){

         $start = (($page-1)*$itemsPerPage);

          $sql = "SELECT * FROM email WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

         $STH = $this->DBH->query($sql);

         $STH->setFetchMode(PDO::FETCH_OBJ);

         $arrSomeData = $STH->fetchAll();

         return $arrSomeData;
     }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM email WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }


}