<?php


namespace App\Hobby;

use App\Message\Message;

use App\Utility\Utility;
use PDO;


use App\Model\Database as DB;

class Hobby extends DB
{
    private $id;

    private $person_name;

    private $Hobbies;

    public function setData($allPostData)
    {
        if (array_key_exists("id", $allPostData)) {


            $this->id = $allPostData['id'];
        }

        if (array_key_exists("name", $allPostData)) {


            $this->person_name = $allPostData['name'];
        }

        if (array_key_exists("hobbies", $allPostData)) {


            $this->Hobbies = $allPostData['hobbies'];
        }
    }

    public function store()
    {

        $Data = array($this->person_name, $this->Hobbies);

        $query = 'INSERT INTO hobby (person_name,hobbies) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($Data);

        if ($result) {

            Message::setMessage("Success!Data has been stored successfully!");

        } else {

            Message::setMessage("Failed!Data has not been stored!");
        }

        utility :: redirect('index.php');


    }

    public function view()
    {

        $sql = "Select * from hobby where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from hobby where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "Select * from hobby where soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update()
    {

         $arrData = array($this->person_name, $this->Hobbies);

         $query = 'UPDATE hobby SET person_name=?,hobbies=? WHERE  id='.$this->id;

         $STH = $this->DBH->prepare($query);

         $result = $STH->execute($arrData);

         if ($result) {

                       Message::setMessage("Success!Data has been updated successfully!");

         }
         else {

                      Message::setMessage("Failed!Data has not been updated!");
        }

                    utility :: redirect('index.php');


    }

    public function trash()
    {

        $arrData = array("Yes");

        $query = 'UPDATE hobby SET soft_deleted=? WHERE  id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been trashed successfully!");

        }
        else {

            Message::setMessage("Failed!Data has not been trashed!");
        }

        utility :: redirect('trashed.php');


    }

    public function recover()
    {

        $arrData = array("No");

        $query = 'UPDATE hobby SET soft_deleted=? WHERE  id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been recovered successfully!");

        }
        else {

            Message::setMessage("Failed!Data has not been recovered!");
        }

        utility :: redirect('index.php');


    }

    public function delete(){

        $sql = "DELETE FROM hobby WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);

        if($result){

            Message::setMessage("Success! Data has been deleted successfully!");
        }

        else{
            Message::setMessage("Failed! Data has not been deleted!");
        }

        utility ::redirect('index.php');
    }

    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM  hobby WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM  hobby WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }




}