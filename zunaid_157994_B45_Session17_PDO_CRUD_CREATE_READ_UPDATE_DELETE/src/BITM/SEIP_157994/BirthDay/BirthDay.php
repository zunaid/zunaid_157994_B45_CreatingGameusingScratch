<?php


namespace App\Birthday;

use App\Message\Message;

use App\Utility\Utility;

use PDO;

use App\Model\Database as DB;

class BirthDay extends DB
{
    private $id;

    private $personName;

    private $BirthDate;



    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){

            $this->id = $allPostData['id'];
        }

        if(array_key_exists("PersonName",$allPostData)){

            $this->personName = $allPostData['PersonName'];
        }

        if(array_key_exists("birthdate",$allPostData)){

            $this->BirthDate = $allPostData['birthdate'];
        }


    }

    public function store(){


        $arrData = array ($this->personName,$this->BirthDate);

        $query =  $query= 'INSERT INTO birth_day (person_name, birth_date) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message :: setMessage("Success! Birth Date has been stored successfully!");
        }
        else{

            Message::setMessage("Failed! Birth Date has not been stored");
        }
        utility :: redirect('index.php');

    }

    public function view()
    {

        $sql = "Select * from birth_day where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from birth_day where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "Select * from birth_day where soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }


    public function update(){


        $arrData = array ($this->personName,$this->BirthDate);

        $query= 'UPDATE  birth_day SET person_name = ?, birth_date = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message :: setMessage("Success! Birth Date has been updated successfully!");
        }
        else{

            Message::setMessage("Failed! Birth Date has not been updated");
        }
        utility :: redirect('index.php');

    }

    public function trash(){


        $arrData = array ("Yes");

        $query ='UPDATE  birth_day SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message :: setMessage("Success! Birth Date has been trashed successfully!");
        }
        else{

            Message::setMessage("Failed! Birth Date has not been trashed");
        }
        utility :: redirect('trashed.php');

    }

    public function recover(){


        $arrData = array ("No");

        $query ='UPDATE  birth_day SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if($result){

            Message :: setMessage("Success! Birth Date has been recovered successfully!");
        }
        else{

            Message::setMessage("Failed! Birth Date has not been recovered");
        }
        utility :: redirect('index.php');

    }

    public function delete(){

        $sql="DELETE FROM birth_day WHERE id=".$this->id;

        $result=$this->DBH->exec($sql);

        if($result){

            Message :: setMessage("Success! Birth Date has been deleted successfully!");
        }
        else{

            Message::setMessage("Failed! Birth Date has not been deleted");
        }

        utility :: redirect('index.php');
    }

    public function indexPaginator($page,$itemsPerPage){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM birth_day WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }

    public function trashedPaginator($page,$itemsPerPage){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM birth_day WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }





}