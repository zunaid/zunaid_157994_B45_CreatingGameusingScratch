<?php


namespace App\OrganizationSummary;

use App\Message\Message;

use App\Utility\Utility;

use PDO;


use App\Model\Database as DB;

class OrganizationSummary extends DB
{
    private $id;

    private $organization_name;

    private $organization_summary;

    public function setData($allPostData)
    {

        if (array_key_exists("id", $allPostData)) {

            $this->id = $allPostData['id'];
        }


        if (array_key_exists("organizationName", $allPostData)) {

            $this->organization_name = $allPostData['organizationName'];
        }


        if (array_key_exists("organizationsummary", $allPostData)) {

            $this->organization_summary = $allPostData['organizationsummary'];
        }


    }

    public function store()
    {

        $arrData = array($this->organization_name, $this->organization_summary);

        $query = 'INSERT INTO organization_information (organization_name, organization_summary) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been stored successfully!");

        } else {

            Message::setMessage("Failed!Data has not been stored!");
        }

        utility :: redirect('index.php');

    }

    public function view()
    {

        $sql = "Select * from organization_information  where id=" . $this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function index(){

        $sql = "Select * from organization_information where soft_deleted='No'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function trashed(){

        $sql = "Select * from organization_information where soft_deleted='Yes'";
        $STH=$this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }

    public function update()
    {

        $arrData = array($this->organization_name, $this->organization_summary);

        $query = 'UPDATE organization_information SET organization_name=?, organization_summary=?, WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been updated successfully!");

        } else {

            Message::setMessage("Failed!Data has not been updated!");
        }

        utility :: redirect('index.php');

    }

    public function trash()
    {

        $arrData = array("Yes");

        $query = 'UPDATE organization_information SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been trashed successfully!");

        } else {

            Message::setMessage("Failed!Data has not been trahed!");
        }

        utility :: redirect('trashed.php');

    }

    public function recover()
    {

        $arrData = array("No");

        $query = 'UPDATE organization_information SET soft_deleted=? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);

        $result = $STH->execute($arrData);

        if ($result) {

            Message::setMessage("Success!Data has been recovered successfully!");

        } else {

            Message::setMessage("Failed!Data has not been recovered!");
        }

        utility :: redirect('index.php');

    }

    public function delete()
    {
        $sql = "Delete FROM organization_information WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if ($result) {

            Message::setMessage("Success!Data has been deleted successfully!");

        } else {

            Message::setMessage("Failed!Data has not been deleted!");
        }

        utility :: redirect('index.php');

    }


    public function indexPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM organization_information WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }

    public function trashedPaginator($page=1,$itemsPerPage=3){

        $start = (($page-1)*$itemsPerPage);

        $sql = "SELECT * FROM organization_information WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData = $STH->fetchAll();

        return $arrSomeData;
    }
}