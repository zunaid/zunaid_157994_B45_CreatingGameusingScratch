<?php



namespace App\CityName;

use App\Message\Message;

use App\Utility\Utility;

use App\Model\Database as DB;

use PDO;

class CityName extends DB

{
     private $id;

     private $Name;

     private $CityName;

     public function setData($allPostData=null){

          if(array_key_exists("id", $allPostData)){

               $this->id = $allPostData['id'];
          }
          if(array_key_exists("bname",$allPostData)){

               $this->Name= $allPostData['bname'];

          }

          if(array_key_exists("cityName",$allPostData)){

               $this->CityName=$allPostData['cityName'];
          }
     }

     public function add(){

          $arrData =array($this->Name, $this->CityName);

          $query = 'INSERT INTO city (person_name,city_name) VALUES (?,?)';

          $STH = $this->DBH->prepare($query);

          $result = $STH->execute($arrData);

          if($result){

               Message::setMessage("Success!City Name has been add successfully!");

          }
          else{

               Message::setMessage("Failed!City Name has not been add!");
          }
          utility :: redirect('index.php');

     }

     public function view()
     {

          $sql = "Select * from city where id=" . $this->id;
          $STH = $this->DBH->query($sql);
          $STH->setFetchMode(PDO::FETCH_OBJ);
          return $STH->fetch();
     }
     public function index(){

          $sql = "Select * from city where soft_deleted='No'";
          $STH=$this->DBH->query($sql);
          $STH->setFetchMode(PDO::FETCH_OBJ);
          return $STH->fetchAll();
     }

     public function trashed(){

          $sql = "Select * from city where soft_deleted='Yes'";
          $STH=$this->DBH->query($sql);
          $STH->setFetchMode(PDO::FETCH_OBJ);
          return $STH->fetchAll();
     }
     public function update(){

          $arrData =array($this->Name,$this->CityName);

          $query = 'UPDATE city SET person_name=?,city_name=? WHERE id='.$this->id;

          $STH = $this->DBH->prepare($query);

          $result = $STH->execute($arrData);

          if($result){

               Message::setMessage("Success!City Name has been updated successfully!");

          }
          else{

               Message::setMessage("Failed!City Name has not been updated!");
          }
          utility :: redirect('index.php');

     }

     public function trash(){

          $arrData =array("Yes");

          $query = 'UPDATE city SET soft_deleted=? WHERE id='.$this->id;

          $STH = $this->DBH->prepare($query);

          $result = $STH->execute($arrData);

          if($result){

               Message::setMessage("Success!City Name has been trashed successfully!");

          }
          else{

               Message::setMessage("Failed!City Name has not been trashed!");
          }
          utility :: redirect('trashed.php');

     }

     public function recover(){

          $arrData =array("No");

          $query = 'UPDATE city SET soft_deleted=? WHERE id='.$this->id;

          $STH = $this->DBH->prepare($query);

          $result = $STH->execute($arrData);

          if($result){

               Message::setMessage("Success!City Name has been recovered successfully!");

          }
          else{

               Message::setMessage("Failed!City Name has not been recovered!");
          }
          utility :: redirect('index.php');

     }

     public function delete(){

          $sql= "DELETE FROM city WHERE id=".$this->id;

          $result = $this->DBH->exec($sql);

          if($result){

               Message::setMessage("Success! City name has been deleted!");
          }

          else{

               Message::setMessage("Failed! City name has not been deleted");
          }

          utility ::redirect('index.php');

     }

     public function indexPaginator($page=1,$itemsPerPage=3){

          $start = (($page-1)*$itemsPerPage);

          $sql = "SELECT * FROM city WHERE soft_deleted= 'No' LIMIT $start,$itemsPerPage ";

          $STH = $this->DBH->query($sql);

          $STH->setFetchMode(PDO::FETCH_OBJ);

          $arrSomeData = $STH->fetchAll();

          return $arrSomeData;
     }

     public function trashedPaginator($page=1,$itemsPerPage=3){

          $start = (($page-1)*$itemsPerPage);

          $sql = "SELECT * FROM city WHERE soft_deleted= 'Yes' LIMIT $start,$itemsPerPage ";

          $STH = $this->DBH->query($sql);

          $STH->setFetchMode(PDO::FETCH_OBJ);

          $arrSomeData = $STH->fetchAll();

          return $arrSomeData;
     }




}