-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2017 at 09:33 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `atomic_project_b45`
--

-- --------------------------------------------------------

--
-- Table structure for table `birth_day`
--

CREATE TABLE `birth_day` (
  `id` int(11) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `birth_date` varchar(111) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birth_day`
--

INSERT INTO `birth_day` (`id`, `person_name`, `birth_date`, `soft_delete`) VALUES
(1, 'shabber', '2015', ''),
(2, 'nnfdknf', '2009', 'No'),
(3, 'lkfdsklf', '1', 'No'),
(4, 'vckvmck', '01/06/1988', 'No'),
(5, 'irjfirjf', '8r83787', 'No'),
(6, 's', '', 'No'),
(7, 'shabber', '09/02/1988', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_delete` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_delete`) VALUES
(0, 'Himu', 'Humaun Ahmed', 'No'),
(0, 'ieioewu', 'dkmxclmvcl', 'No'),
(0, 'oeireoiwr', ',mnb,mvbn', 'No'),
(0, 'lllll', 'zunaud', 'No'),
(0, 'hhhh', 'ssss', 'No'),
(0, 'ksjndjndskjf', 'mvcmvc,v,v', 'No'),
(0, 'Himi', 'Humaun Ahmed', 'No'),
(0, 'Himu', 'Humaun Ahmed', 'No'),
(0, 'sssdfdff', 'dfdfdfdfd', 'No'),
(0, 'Himu', 'Humaun Ahmed', 'No'),
(0, 'ssdffff', 'gggggggg', 'No'),
(0, 'kdmff', 'cv.cv,.v,c.', 'No'),
(0, 'dnnfdfn', 'd,d,', 'No'),
(0, 'lllllll', 'jjjjjjj', 'No'),
(0, 'ssldkfjdk', 'mbvbmkgbmg', 'No'),
(0, 'lkdlfkld', 'cnvcnvfnv', 'No'),
(0, 'kdfdkf', 'cmv,cxv', 'No'),
(0, 'himu ', 'Humaun Ahmed', 'No'),
(0, 'dfdkfj', 'oeiureiwur', 'No'),
(0, 'FKLGJFKG', 'ITREIUTO', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` text COLLATE latin1_general_ci NOT NULL,
  `city_name` varchar(111) COLLATE latin1_general_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE latin1_general_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city_name`, `soft_deleted`) VALUES
(1, 'hgvvhv', 'khulna', 'Yes'),
(2, 'hdjhfdjh', 'ctg', 'No'),
(3, 'irreiotor', 'dhaka', 'No'),
(4, 'wwww', 'chittagong', 'No'),
(5, 'rrr', 'dhaka', 'Yes'),
(6, 'kkkkkk', 'chittagong', 'No'),
(7, 'kfgflkg', 'chittagong', 'Yes'),
(8, 'karim', 'chittagong', 'No'),
(9, 'rahim', 'khulna', 'Yes'),
(10, 'sumon', 'sylhet', 'No'),
(11, 'sumon', 'sylhet', 'No'),
(12, 'lkkllk', 'rajsahahi', 'No'),
(13, 'zunaid', 'chittagong', 'No'),
(14, 'lita', 'khulna', 'No'),
(15, 'lutfur', 'rajsahahi', 'No'),
(16, 'faruk', 'sylhet', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'kkkkk', 'dddddd', 'No'),
(2, 'zunaid', 'zunaidcse59@gmail.com', 'Yes'),
(3, 'kdsfdkfj', 'zunaidcse59@gmail.com', 'No'),
(4, 'kkkkkkk', 'zunaidcse59@gmail.com', 'No'),
(5, 'zunaid', 'zunaidcse59@gmail.com', 'Yes'),
(6, 'rahim', 'rahim@gmail.com', 'No'),
(7, 'rafiq', 'rafiq@yahoo.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE `gender` (
  `id` int(11) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `person_name`, `gender`, `soft_deleted`) VALUES
(1, 'wwww', 'male', 'No'),
(2, 'kkkk', 'male', 'No'),
(3, 'fgfgg', 'male', 'Yes'),
(4, 'kkkk', 'male', 'No'),
(5, 'kkkk', 'male', 'Yes'),
(6, 'ggg', 'male', 'No'),
(7, 'kfkgkfgkf', 'female', 'Yes'),
(8, 'shabber', 'male', 'No'),
(9, 'rahim', 'male', 'No'),
(10, 'rahima', 'female', 'No'),
(11, 'rahima', 'female', 'No'),
(12, 'rahima', 'female', 'No'),
(13, 'samiul', 'male', 'No'),
(14, 'samiul', 'male', 'No'),
(15, 'samiul', 'male', 'No'),
(16, 'safi', 'male', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `hobby`
--

CREATE TABLE `hobby` (
  `id` int(11) NOT NULL,
  `person_name` varchar(111) NOT NULL,
  `hobbies` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobby`
--

INSERT INTO `hobby` (`id`, `person_name`, `hobbies`, `soft_deleted`) VALUES
(1, 'lkkkk', 'Books', 'No'),
(2, 'shabber', 'Games', 'No'),
(5, 'rafiq', 'Books', 'Yes'),
(6, 'Halim', 'Books', 'No'),
(7, 'rahima', 'Books', 'Yes'),
(8, 'samiul', 'Travelling', 'No'),
(9, 'sorowar', 'Movies', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_information`
--

CREATE TABLE `organization_information` (
  `id` int(11) NOT NULL,
  `organization_name` varchar(111) NOT NULL,
  `organization_summary` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization_information`
--

INSERT INTO `organization_information` (`id`, `organization_name`, `organization_summary`, `soft_deleted`) VALUES
(1, 'llfglfg', 'dkjkdj ncjdsnfjd ', 'No'),
(2, 'M,XVN,V,VNMSD', 'dfjkdfkjdkfdkf', 'No'),
(3, 'bbbbbbb', 'jjjjjjjj', 'Yes'),
(4, 'llllllll', 'ssssssssss', 'Yes'),
(5, 'oookokokoko', 'njnjnjnjnjnj', 'No'),
(6, 'fsdfsdfsdfsfs', 'cxzvcdddfsfsdsdfd', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `profile_pic` text COLLATE latin1_general_ci NOT NULL,
  `soft_delete` varchar(11) COLLATE latin1_general_ci NOT NULL DEFAULT 'No'
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birth_day`
--
ALTER TABLE `birth_day`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobby`
--
ALTER TABLE `hobby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_information`
--
ALTER TABLE `organization_information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birth_day`
--
ALTER TABLE `birth_day`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `hobby`
--
ALTER TABLE `hobby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `organization_information`
--
ALTER TABLE `organization_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
