<?php
require_once("../../../../vendor/autoload.php");

$objOrganizationSummary = new \App\OrganizationSummary\OrganizationSummary();

$allData = $objOrganizationSummary->trashed();

use App\Message\Message;

use App\Utility\Utility;


if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div style='height: 30px'> <div  id='message'> $msg </div> </div>";

############## Pagination Block 1 of 2 Start ##################


$recordCount= count($allData);


if(isset($_REQUEST['Page']))   $page = $_REQUEST['Page'];
else if(isset($_SESSION['Page']))   $page = $_SESSION['Page'];
else   $page = 1;


$_SESSION['Page']= $page;

if(isset($_REQUEST['ItemsPerPage']))   $itemsPerPage = $_REQUEST['ItemsPerPage'];
else if(isset($_SESSION['ItemsPerPage']))   $itemsPerPage = $_SESSION['ItemsPerPage'];
else   $itemsPerPage = 3;
$_SESSION['ItemsPerPage']= $itemsPerPage;

$pages = ceil($recordCount/$itemsPerPage);
$someData = $objOrganizationSummary->trashedPaginator($page,$itemsPerPage);



$serial = (  ($page-1) * $itemsPerPage ) +1;



if($serial<1) $serial=1;


##################### End Pagination Block 1 of 2 ####################################

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Organization - Trashed List</title>
    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../../resource/bootstrap/js/bootstrap.min.js"></script>

    <style>
        table{
            height: 200px;
            width: 30%;
        }
        tr{
            text-align: center;
        }
        th{
            text-align: center;
        }
        td{
            text-align: center;
        }

    </style>
</head>
<body>
<div class="container">
<h1 align="center">Organization - Trashed List &nbsp;&nbsp;
    <a href='index.php' class='btn btn-lg btn-default'>Go To Active List</a>
</h1>
<table class="table table-striped table-bordered" cellspacing="0" cellpadding="2" align="center">
       <tr>
           <th>Serial No.</th>
           <th>ID</th>
           <th>Organization Name</th>
           <th>Summary</th>
           <th>Action Buttons</th>
       </tr>

<?php

      //$serial = 1;

      foreach($someData as $oneData){
          if($serial%2)$bgColor="#cccccc";
          else $bgColor = "#ffffff";

          echo "
                <tr style='background-color: $bgColor'>
                    <td>$serial</td>
                    <td>$oneData->id</td>
                    <td>$oneData->organization_name</td>
                    <td>$oneData->organization_summary</td>
                    <td><a href='view.php?id=$oneData->id' class='btn btn-info'>View</a>
                        <a href='recover.php?id=$oneData->id' class='btn btn-success'>Recover</a>
                    </td>
                </tr>
              ";
          $serial++;
      }


?>



</table>
</div>

<!-------##################### Pagination Block 2 of 2 Start ####################################------------>

<div align="left" class="container">
    <ul class="pagination">

        <?php

        $pageMinusOne  = $page-1;
        $pagePlusOne  = $page+1;


        if($page>$pages) Utility::redirect("trashed.php?Page=$pages");

        if($page>1)  echo "<li><a href='trashed.php?Page=$pageMinusOne'>" . "Previous" . "</a></li>";


        for($i=1;$i<=$pages;$i++)
        {
            if($i==$page) echo '<li class="active"><a href="">'. $i . '</a></li>';
            else  echo "<li><a href='?Page=$i'>". $i . '</a></li>';

        }
        if($page<$pages) echo "<li><a href='trashed.php?Page=$pagePlusOne'>" . "Next" . "</a></li>";

        ?>

        <select  class="form-control"  name="ItemsPerPage" id="ItemsPerPage" onchange="javascript:location.href = this.value;" >
            <?php
            if($itemsPerPage==3 ) echo '<option value="?ItemsPerPage=3" selected >Show 3 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=3">Show 3 Items Per Page</option>';

            if($itemsPerPage==4 )  echo '<option  value="?ItemsPerPage=4" selected >Show 4 Items Per Page</option>';
            else  echo '<option  value="?ItemsPerPage=4">Show 4 Items Per Page</option>';

            if($itemsPerPage==5 )  echo '<option  value="?ItemsPerPage=5" selected >Show 5 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=5">Show 5 Items Per Page</option>';

            if($itemsPerPage==6 )  echo '<option  value="?ItemsPerPage=6"selected >Show 6 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=6">Show 6 Items Per Page</option>';

            if($itemsPerPage==10 )   echo '<option  value="?ItemsPerPage=10"selected >Show 10 Items Per Page</option>';
            else echo '<option  value="?ItemsPerPage=10">Show 10 Items Per Page</option>';

            if($itemsPerPage==15 )  echo '<option  value="?ItemsPerPage=15"selected >Show 15 Items Per Page</option>';
            else    echo '<option  value="?ItemsPerPage=15">Show 15 Items Per Page</option>';
            ?>
        </select>
    </ul>
</div>

<!-----##################### End Pagination Block 2 of 2 #################################### ------>


</body>
</html>
