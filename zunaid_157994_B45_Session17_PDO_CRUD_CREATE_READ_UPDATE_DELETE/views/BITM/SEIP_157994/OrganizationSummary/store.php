<?php

require_once ("../../../../vendor/autoload.php");

use App\OrganizationSummary\OrganizationSummary;


$objOrganizationSummary = new OrganizationSummary();

$objOrganizationSummary->setData($_POST);

$objOrganizationSummary->store();