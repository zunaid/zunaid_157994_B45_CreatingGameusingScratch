<?php

require_once("../../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)) session_start();

$msg = Message::getMessage();
echo "<div id='message'></div>";

$_SESSION['message'] ="";

$objEmail = new \App\Email\Email();
$objEmail->setData($_GET);
$oneData = $objEmail->view();

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Email Edit Form</title>

    <script
        src="https://code.jquery.com/jquery-3.1.1.slim.min.js"
        integrity="sha256-/SIrNqv8h6QGKDuNoLGA4iret+kyesCkHGzVUUV0shc="
        crossorigin="anonymous"></script>

</head>
<body>
     <form action="update.php" method="post">
            Pls Enter Your Name :
            <input type="text" name="Name"><br>
            Pls Enter Your Email :
            <input type="text" name="EMail"><br>
            <input type="hidden" name="id" value="<?php echo $oneData->id;?>">
            <input type="submit" value="Update">
    </form>

     <script src="../../../resource/bootstrap/js/jquery-1.11.1.min.js"></script>

     <script>
         jQuery(function($) {
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
             $('#message').fadeIn (550);
             $('#message').fadeOut (550);
         })
         </script>

</body>
</html>